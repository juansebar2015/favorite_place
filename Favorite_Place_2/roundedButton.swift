//
//  roundedButton.swift
//  Favorite_Place_2
//
//  Created by Juan Ramirez on 5/5/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class roundedButton: UIButton {

    override func awakeFromNib() {
        
        self.layer.cornerRadius = 15.0
        self.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.backgroundColor = UIColor(red: 252.0/255.0, green: 110.0/255.0, blue: 81.0/255.0, alpha: 1.0)
    }

}
