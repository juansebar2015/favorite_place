//
//  ViewController.swift
//  Favorite_Place_2
//
//  Created by Juan Ramirez on 5/4/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var aboutButton: UIButton!
    @IBOutlet weak var photosButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Modify button atributes
        aboutButton.layer.cornerRadius = 15.0
        photosButton.layer.cornerRadius = 15.0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

